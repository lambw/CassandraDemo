import org.apache.hadoop.mapreduce.RecordReader;
import java.io.IOException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.cassandra.hadoop.ConfigHelper;
import org.apache.cassandra.hadoop.HadoopCompat;
import org.apache.cassandra.hadoop.cql3.CqlRecordReader;
import org.apache.cassandra.utils.Pair;
import com.datastax.driver.core.Token;

public class CfCqlRecordReader extends RecordReader<Long, CfRow>
    implements org.apache.hadoop.mapred.RecordReader<Long, CfRow> {
    private static final Logger logger = LoggerFactory.getLogger(CfCqlRecordReader.class);

    private String[] columnFamilies;
    private ArrayList<CqlRecordReader> cfRecordReaders = new ArrayList<CqlRecordReader>();

    private Pair<Long, CfRow> currentRow;
    private Token currentRowToken;
    private Long totalRead;

    public CfCqlRecordReader()
    {
        super();
    }

    @Override
    public boolean nextKeyValue() throws IOException, InterruptedException
    {
      int currentLineTotal = 0;
      Boolean isParellel = false;

      while (!isParellel)
      {
          for (CqlRecordReader cfRecordReader : cfRecordReaders)
          {
              if(isParellel) break;
              while(true)
              {
                  if (!cfRecordReader.nextKeyValue())
                  {
                        logger.trace("Finished scanning {} rows", totalRead);
                        return false;
                  }
                  Token token = cfRecordReader.getCurrentValue().getPartitionKeyToken();
                  if(currentRowToken != null && token.compareTo(currentRowToken) < 0)
                      continue;
                  if (currentRowToken == null || token.compareTo(currentRowToken) > 0)
                  {
                      currentRowToken = token;
                      currentLineTotal = 1;
                  }
                  else if(token.compareTo(currentRowToken) == 0)
                  {
                      currentLineTotal++;
                  }
                  if(currentLineTotal == columnFamilies.length)
                  {
                      isParellel = true;
                  }
                  break;
                }
          }
      }

      CfRowInner mergeRow = new CfRowInner();
      for (int i = 0; i < columnFamilies.length; ++i)
      {
    	  mergeRow.add(columnFamilies[i], cfRecordReaders.get(i).getCurrentValue());
      }
      currentRow = Pair.create(totalRead++, mergeRow);
      return true;
    }

    @Override
    public Long getCurrentKey() throws IOException, InterruptedException
    {
        return currentRow.left;
    }

    @Override
    public CfRow getCurrentValue() throws IOException, InterruptedException
    {
        return currentRow.right;
    }

    @Override
    public boolean next(Long key, CfRow value) throws IOException
    {
      try
      {
    	  if (nextKeyValue())
		  {
    		  value = getCurrentValue();
    		  return true;
    	  }
      } catch (InterruptedException e) {
		  logger.trace(e.toString());
	  }
      return false;
    }

    @Override
    public Long createKey()
    {
        return Long.valueOf(0L);
    }

    @Override
    public CfRow createValue()
    {
        return new CfRowInner();
    }

    @Override
    public long getPos() throws IOException
    {
        return totalRead;
    }

    @Override
    public void close() throws IOException
    {
        for (CqlRecordReader cfRecordReader : cfRecordReaders)
        {
            cfRecordReader.close();
        }
    }

    @Override
    public float getProgress() throws IOException
    {
    	float progess = 0.0F;
    	for(CqlRecordReader cfRecordReader : cfRecordReaders)
    	{
    		progess = Math.max(progess, cfRecordReader.getProgress());
    	}
    	return progess;
    }

	public void initialize(InputSplit s, TaskAttemptContext tac) throws IOException, InterruptedException
    {
        Configuration conf        = HadoopCompat.getConfiguration(tac);
        String        keyspace    = ConfigHelper.getInputKeyspace(conf);
        String        oldCfConfig = ConfigHelper.getInputColumnFamily(conf);

        columnFamilies = oldCfConfig.split(";");
        totalRead = 0L;
        currentRowToken = null;

        for (int i = 0; i < columnFamilies.length; ++i)
        {
            // CqlRecordReader set column family by job Configuration
            ConfigHelper.setInputColumnFamily(conf, keyspace, columnFamilies[i]);
            CqlRecordReader cfRecordReader = new CqlRecordReader();
            cfRecordReader.initialize(s, tac);
            cfRecordReaders.add(cfRecordReader);
        }
        ConfigHelper.setInputColumnFamily(conf, keyspace, oldCfConfig);
    }
}
